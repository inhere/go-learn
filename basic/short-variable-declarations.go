package main

import "fmt"

func main() {
	var i, j = 1, 2
	k := 3
	// var c, php = false, "no !"
	c, php := false, "no !"

	fmt.Println(i, j, k, c, php)
}

/*
短声明变量
在函数中， := 简洁赋值语句在明确类型的地方，可以用于替代 var 定义。

函数外的每个语句都必须以关键字开始（ var 、 func 、等等）， := 结构不能使用在函数外。
*/
