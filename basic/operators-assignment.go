package main

import "fmt"

func main() {
	var a int = 21
	var c int

	c = a
	fmt.Printf("Line 1 - =  Operator Example, Value of c = %d\n", c)

	c += a
	fmt.Printf("Line 2 - += Operator Example, Value of c = %d\n", c)

	c -= a
	fmt.Printf("Line 3 - -= Operator Example, Value of c = %d\n", c)

	c *= a
	fmt.Printf("Line 4 - *= Operator Example, Value of c = %d\n", c)

	c /= a
	fmt.Printf("Line 5 - /= Operator Example, Value of c = %d\n", c)

	c = 200

	c <<= 2
	fmt.Printf("Line 6 - <<= Operator Example, Value of c = %d\n", c)

	c >>= 2
	fmt.Printf("Line 7 - >>= Operator Example, Value of c = %d\n", c)

	c &= 2
	fmt.Printf("Line 8 - &= Operator Example, Value of c = %d\n", c)

	c ^= 2
	fmt.Printf("Line 9 - ^= Operator Example, Value of c = %d\n", c)

	c |= 2
	fmt.Printf("Line 10 - |= Operator Example, Value of c = %d\n", c)

}

/*
Go语言支持以下赋值运算符：

运算符 描述  示例
=   简单的赋值操作符，分配值从右边的操作数左侧的操作数   C = A + B 将分配A + B的值到C
+=  相加并赋值运算符，它增加了右操作数左操作数和分配结果左操作数  C += A 相当于 C = C + A
-=  减和赋值运算符，它减去右操作数从左侧的操作数和分配结果左操作数 C -= A 相当于 C = C - A
*=  乘法和赋值运算符，它乘以右边的操作数与左操作数和分配结果左操作数    C *= A is equivalent to C = C * A
/=  除法赋值运算符，它把左操作数与右操作数和分配结果左操作数    C /= A 相当于 C = C / A
%=  模量和赋值运算符，它需要使用两个操作数的模量和分配结果左操作数 C %= A 相当于 C = C % A
<<= 左移位并赋值运算符   C <<= 2 相同于 C = C << 2
>>= 向右移位并赋值运算符  C >>= 2 相同于 C = C >> 2
&=  按位与赋值运算符    C &= 2 相同于 C = C & 2
^=  按位异或并赋值运算符  C ^= 2 相同于 C = C ^ 2
|=  按位或并赋值运算符   C |= 2 相同于 C = C | 2
*/
