package main

import "fmt"

var i, j int = 1, 3

func main() {
	var c, php, java = true, false, "no!"
	fmt.Println(i, j, c, php, java)
}

/*
初始化变量
变量定义可以包含初始值，每个变量对应一个。

如果初始化是使用表达式，则可以省略类型；**变量从初始值中获得类型**。
*/
