// 多值返回

package main

import "fmt"

func main() {
	fmt.Println("hello")

	a, b := swap("hello", "world")

	fmt.Println(a, b)
}

func swap(x, y string) (string, string) {
	return y, x
}
