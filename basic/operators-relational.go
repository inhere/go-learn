package main

import "fmt"

func main() {
	var a int = 21
	var b int = 10

	if a == b {
		fmt.Printf("Line 1 - a is equal to b\n")
	} else {
		fmt.Printf("Line 1 - a is not equal to b\n")
	}
	if a < b {
		fmt.Printf("Line 2 - a is less than b\n")
	} else {
		fmt.Printf("Line 2 - a is not less than b\n")
	}

	if a > b {
		fmt.Printf("Line 3 - a is greater than b\n")
	} else {
		fmt.Printf("Line 3 - a is not greater than b\n")
	}
	/* Lets change value of a and b */
	a = 5
	b = 20
	if a <= b {
		fmt.Printf("Line 4 - a is either less than or equal to  b\n")
	}
	if b >= a {
		fmt.Printf("Line 5 - b is either greater than  or equal to b\n")
	}
}

/*
下表列出了所有Go语言支持的关系运算符。假设变量A=10和变量B=20，则：

运算符 描述  示例
==  检查两个操作数的值是否相等，如果是的话那么条件为真。  (A == B) 不为 true.
!=  检查两个操作数的值是否相等，如果值不相等，则条件变为真。    (A != B) 为true.
>   检查左边的操作数的值是否大于右操作数的值，如果是的话那么条件为真。   (A > B) 不为 true.
<   检查左边的操作数的值是否小于右操作数的值，如果是的话那么条件为真。   (A < B) 为 true.
>=  检查左边的操作数的值是否大于或等于右操作数的值，如果是的话那么条件为真。    (A >= B) 不为 true.
<=  检查左边的操作数的值是否小于或等于右操作数的值，如果是的话那么条件为真。    (A <= B) 为 true.
*/
