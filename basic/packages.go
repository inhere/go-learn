package main

// 按照惯例，包名与导入路径的最后一个目录一致。
// 例如，"math/rand" 包由 package rand 语句开始。
import (
	"fmt"
	"math/rand"
)

// 注意：这个程序的运行环境是确定性的，因此 rand.Intn 每次都会返回相同的数字。
// （为了得到不同的随机数，需要提供一个随机数种子，参阅 rand.Seed。）
func main() {
	// rand.Seed(3455)
	fmt.Println("my favorite number is ", rand.Intn(10))
}
