package main

import "fmt"

func main() {

	var a int = 21
	var b int = 10
	var c int

	c = a + b
	fmt.Printf("Line 1 - Value of c is %d\n", c)
	c = a - b
	fmt.Printf("Line 2 - Value of c is %d\n", c)
	c = a * b
	fmt.Printf("Line 3 - Value of c is %d\n", c)
	c = a / b
	fmt.Printf("Line 4 - Value of c is %d\n", c)
	c = a % b
	fmt.Printf("Line 5 - Value of c is %d\n", c)
	a++
	fmt.Printf("Line 6 - Value of a is %d\n", a)
	a--
	fmt.Printf("Line 7 - Value of a is %d\n", a)
}

/*
Go语言运算符优先级
运算符优先级决定术语的表达分组。这会影响一个表达式是如何进行评估计算。某些运算符的优先级高于其他;例如，乘法运算符的优先级比所述加法运算更高。

例如X =7 + 3* 2;这里，x被赋值13，而不是20，因为操作员*具有优先级高于+，所以它首先被乘以3 * 2，然后相加上7。

这里，具有最高优先级的操作出现在表的顶部，那些具有最低出现在底部。在一个表达式，更高的优先级运算符将首先评估计算。

运算符优先级示例

分类  运算符     关联
后缀  () [] -> . ++ - -   从左到右
一元  + - ! ~ ++ - - (type)* & sizeof     从右到左
乘法  * / %   从左到右
相加      + -     从左到右
移动  << >>   从左到右
关系  < <= > >=   从左到右
相等  == !=   从左到右
按位与 &   从左到右
按位异或    ^   从左到右
按位或     |   从左到右
逻辑与     &&  从左到右
逻辑或     ||  从左到右
条件  ?:  从左到右
赋值  = += -= *= /= %=>>= <<= &= ^= |=    从右到左
逗号  ,   从左到右
*/
