package main

import "fmt"

func main() {

	// 先转换成值的对应二进制
	var a uint = 60 /* 60 = 0011 1100 */
	var b uint = 13 /* 13 = 0000 1101 */
	var c uint = 0

	c = a & b /* c = 12 = 0000 1100; & 取两个操作数都存在的部分 */
	fmt.Printf("Line 1 - Value of c is %d\n", c)

	c = a | b /* c = 61 = 0011 1101; & 取两个操作数任意一个存在的部分 */
	fmt.Printf("Line 2 - Value of c is %d\n", c)

	c = a ^ b /* 49 = 0011 0001; 异或，只存在一个操作数中的部分 */
	fmt.Printf("Line 3 - Value of c is %d\n", c)

	c = a << 2 /* 240 = 1111 0000 ; 操作数的值(0011 1100)左移动两位 */
	fmt.Printf("Line 4 - Value of c is %d\n", c)

	c = a >> 2 /* 15 = 0000 1111; 操作数的值(0011 1100)右移动两位 */
	fmt.Printf("Line 5 - Value of c is %d\n", c)
}

/*
位运算符适用于位并进行逐位操作。 如下 &, |, ^ 的真值表

p   q   p & q   p | q   p ^ q
0   0     0       0       0
0   1     0       1       1
1   1     1       1       0
1   0     0       1       1

假设，如果A =60;且b =13;现在以二进制格式它们如下：

A = 0011 1100

B = 0000 1101

-----------------

A&B = 0000 1100

A|B = 0011 1101

A^B = 0011 0001

~A  = 1100 0011

C语言支持位运算符列在如下表。假设变量A=60和变量B=13，则：

运算  描述  示例
&   二进制与操作副本位的结果，如果它存在于两个操作数    (A & B) = 12, 也就是 0000 1100
|   二进制或操作副本，如果它存在一个操作数 (A | B) = 61, 也就是 0011 1101
^   二进制异或操作副本，如果它被设置在一个操作数但不能同时是存在  (A ^ B) = 49, 也就是 0011 0001
<<  二进制左移位运算符。左边的操作数的值向左移动由右操作数指定的位数    A << 2 will give 240 也就是 1111 0000
>>  二进制向右移位运算符。左边的操作数的值由右操作数指定的位数向右移动   A >> 2 = 15 也就是 0000 1111
*/
