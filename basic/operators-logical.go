package main

import "fmt"

func main() {
	var a bool = true
	var b bool = false
	if a && b {
		fmt.Printf("Line 1 - Condition is true\n")
	}
	if a || b {
		fmt.Printf("Line 2 - Condition is true\n")
	}
	/* lets change the value of  a and b */
	a = false
	b = true
	if a && b {
		fmt.Printf("Line 3 - Condition is true\n")
	} else {
		fmt.Printf("Line 3 - Condition is not true\n")
	}
	if !(a && b) {
		fmt.Printf("Line 4 - Condition is true\n")
	}
}

/*
下表列出了所有Go语言支持的逻辑运算符。假设变量A=1和变量B=0，则：

运算符 描述  示例
&&  所谓逻辑与运算符。如果两个操作数都非零，则条件变为真。 (A && B) 为 false.
||  所谓的逻辑或操作。如果任何两个操作数是非零，则条件变为真。   (A || B) 为 true.
!   所谓逻辑非运算符。使用反转操作数的逻辑状态。如果条件为真，那么逻辑非操后结果为假。   !(A && B) 为 true.
*/
