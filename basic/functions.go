package main

import "fmt"

// 注意类型在变量名 之后 。 这样 相同类型的参数 可在最后一个同类型参数后定义类型
// func add(x int, y int) int {
func add(x, y int) int {
	return x + y
}

func main() {
	fmt.Println(add(5, 8))
}
