package main

import "fmt"

func main() {
	pow := make([]int, 10)

	fmt.Println(pow)

	for i := range pow {
		pow[i] = 1 << uint(i)
	}

	for _, val := range pow {
		fmt.Printf("%d\n", val)
	}
}

/*
OUT:
  [0 0 0 0 0 0 0 0 0 0]
  1
  2
  4
  8
  16
  32
  64
  128
  256
  512

range（续）
可以通过赋值给 _ 来忽略序号和值。

如果只需要索引值，去掉 “ , value ” 的部分即可。
*/
