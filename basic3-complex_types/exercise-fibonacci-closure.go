package main

import "fmt"

// fibonacci 函数会返回一个返回 int 的函数。
// 0、1、1、2、3、5、8、13、21、34 ... 这个数列从第2项开始，每一项都等于前两项之和。
func fibonacci() func() int {
	prev, next := 0, 1

	return func() int {

		old := next
		next = next + prev
		prev = old
		// prev, next := next, prev + next

		return next
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}

/*

练习：斐波纳契闭包
现在来通过函数做些有趣的事情。

实现一个 fibonacci 函数，返回一个函数（一个闭包）可以返回连续的斐波纳契数。
*/
