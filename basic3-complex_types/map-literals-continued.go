package main

import (
	"fmt"
)

type Vertex struct {
	Lat, Long float64
}

/*
变量 m 的
类型是: `map[string]Vertex`
值是:
```
{
    "Bell Labs": {40.68433, -74.39967},
    "Google":    {37.42202, -122.08408},
}
```
*/
var m = map[string]Vertex{
	"Bell Labs": {40.68433, -74.39967},
	"Google":    {37.42202, -122.08408},
}

func main() {
	fmt.Println(m)
}

/*
OUT:
  map[Bell Labs:{40.68433 -74.39967} Google:{37.42202 -122.08408}]

map 的文法（续）

如果顶级的类型只有类型名的话，可以在文法的元素中省略键名。
*/
