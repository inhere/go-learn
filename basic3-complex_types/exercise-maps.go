package main

import (
	// "fmt"
	"golang.org/x/tour/wc"
	"strings"
)

func WordCount(s string) map[string]int {
	// fmt.Println(s)
	// fmt.Printf("Type %T\n", strings.Fields(s)) // Type []string

	box := make(map[string]int)
	// strings.Fields(s) 以一个(或多个连续的)空格分割一个字符串
	list := strings.Fields(s)

	for _, word := range list {
		v, ok := box[word]
		if ok {
			box[word] = v + 1
		} else {
			box[word] = 1
		}
	}

	//return map[string]int{"x": 1}
	return box
}

func main() {
	wc.Test(WordCount)

	// var ret map[string]int

	// ret = WordCount("I am learning Go!")
	// fmt.Println(ret)
	// ret = WordCount("The quick brown fox jumped over the lazy dog.")
	// fmt.Println(ret)
	// ret = WordCount("I ate a donut. Then I ate another donut.")
	// fmt.Println(ret)
	// ret = WordCount("A man a plan a canal panama.")
	// fmt.Println(ret)
}

/*


练习：map
实现 WordCount。它应当返回一个含有 s 中每个 “词” 个数的 map。函数 wc.Test 针对这个函数执行一个测试用例，并输出成功还是失败。

你会发现 [strings.Fields](https://go-zh.org/pkg/strings/#Fields) 很有帮助。
*/
