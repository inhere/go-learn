## 构建和测试程序

```
echo $GOPATH
~/Workspace/go
go build go-learn/exercises/sorter/algorithms/bubblesort
go test go-learn/exercises/sorter/algorithms/bubblesort
go build go-learn/exercises/sorter/algorithms/qsort
go test go-learn/exercises/sorter/algorithms/qsort
go install go-learn/exercises/sorter/algorithms/bubblesort
go install go-learn/exercises/sorter/algorithms/qsort

go build go-learn/exercises/sorter/sorter
go install go-learn/exercises/sorter/sorter
```

## 运行

```
./sorter -i unsorted.dat -o sorted.dat -a bubblesort
./sorter -i unsorted.dat -o sorted.dat -a qsort
```
