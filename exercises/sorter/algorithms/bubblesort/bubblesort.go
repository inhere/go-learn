// bubblesort.go
package bubblesort

// import "fmt"

func BubbleSort(values []int) {
	flag, len := true, len(values)

	for i := 0; i < len-1; i++ {
		flag = true

		for j := 0; j < len-i-1; j++ {
			if values[j] > values[j+1] {
				values[j], values[j+1] = values[j+1], values[j]
				flag = false
			}

		} // end for j

		if flag == true {
			break
		}
	} // end for i

}
