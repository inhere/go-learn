// exercise-web-crawler.go
package main

import (
	"errors"
	"fmt"
	"sync"
)

type Fetcher interface {
	// 通过url 抓取url所在页面的数据(body)，同时返回此页面的子urls
	Fetch(url string) (body string, urls []string, err error)
}

/*
定义变量并初始化
存储了已经抓取或正在抓取的urls
当正在读取或写入时需要锁定
*/
var fetched = struct {
	m map[string]error
	sync.Mutex
}{
	m: make(map[string]error),
}

var loading = errors.New("url load in progress")

// 并行的抓取 URL。
// 不重复抓取页面。
func Crawl(url string, depth int, fetcher Fetcher) {

	if depth <= 0 {
		return
	}

	fetched.Lock()
	// 已抓取过
	if _, ok := fetched.m[url]; ok {
		fetched.Unlock()
		fmt.Printf("<- Done with %v, already fetched.\n", url)
		return
	}

	// 标记当前正在抓取的url, 并设置消息为进行中
	fetched.m[url] = loading
	fetched.Unlock()

	// 抓取数据并赋值到变量
	body, urls, err := fetcher.Fetch(url)

	fetched.Lock()
	fetched.m[url] = err // 设置消息为 抓取的结果消息
	fetched.Unlock()

	// 有错误
	if err != nil {
		fmt.Printf("<- Error on %v: %v\n", url, err)
		return
	}

	fmt.Printf("found: %s %q\n", url, body)
	done := make(chan bool)
	for i, u := range urls {
		fmt.Printf("-> Crawling child %v/%v of %v : %v.\n", i, len(urls), url, u)
		go func(url string) {
			Crawl(u, depth-1, fetcher)
			done <- true
		}(u)
	}

	for i, u := range urls {
		fmt.Printf("<- [%v] %v/%v Waiting for child %v.\n", url, i, len(urls), u)
		<-done
	}

	fmt.Printf("<- Done with %v\n", url)
}

func main() {
	Crawl("http://golang.org/", 4, fetcher)

	fmt.Println("Fetching stats\n -------------")
	for url, err := range fetched.m {
		if err != nil {
			fmt.Printf("%v failed: %v\n", url, err)
		} else {
			fmt.Printf("%v was fetched\n", url)
		}
	}
}

type fakeResult struct {
	body string
	urls []string
}

type fakeFetcher map[string]*fakeResult

func (f *fakeFetcher) Fetch(url string) (string, []string, error) {
	if res, ok := (*f)[url]; ok {
		return res.body, res.urls, nil
	}

	return "", nil, fmt.Errorf("not found: %s", url)
}

var fetcher = &fakeFetcher{
	"http://golang.org/": &fakeResult{
		"The Go Programming Language",
		[]string{
			"http://golang.org/pkg/",
			"http://golang.org/cmd/",
		},
	},
	"http://golang.org/pkg/": &fakeResult{
		"Packages",
		[]string{
			"http://golang.org/",
			"http://golang.org/cmd/",
			"http://golang.org/pkg/fmt/",
			"http://golang.org/pkg/os/",
		},
	},
	"http://golang.org/pkg/fmt/": &fakeResult{
		"Package fmt",
		[]string{
			"http://golang.org/",
			"http://golang.org/pkg/",
		},
	},
	"http://golang.org/pkg/os/": &fakeResult{
		"Package os",
		[]string{
			"http://golang.org/",
			"http://golang.org/pkg/",
		},
	},
}

/*

练习：Web 爬虫
在这个练习中，将会使用 Go 的并发特性来并行执行 web 爬虫。

修改 Crawl 函数来并行的抓取 URLs，并且保证不重复。

提示：你可以用一个 map 来缓存已经获取的 URL，但是需要注意 map 本身并不是并发安全的！
*/
