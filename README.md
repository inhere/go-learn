# go-learn

- 基础
    - 包 变量 函数
    - 流程控制语句
    - 复杂类型
- 方法和接口
- 并发


## 练习

- 在线练习网站： [tour.go-zh.org](https://tour.go-zh.org)
- sublime text 3 (安装GoSublime插件)