// exercise-errors.go
package main

import (
	"fmt"
)

type ErrSqrt float64

func (e ErrSqrt) Error() string {
	return fmt.Sprint("cannot Sqrt negative number: ", float64(e))
}

func Sqrt(x float64) (float64, error) {

	if x <= 0 {
		err := ErrSqrt(x)

		return 0, err
	}

	z, d := 1.0, 1000.0

	// 无限循环
	for {
		old := z
		z = z - (z*z-x)/(2*z)
		// z = z + (x/z-z)/2

		// 当本次计算结果和上次结果 在小数点后d位相同时停止
		if uint(z*d) == uint(old*d) {
			return z, nil
		}

		// fmt.Println(z)
	}

	return 0, nil
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(-2))
}
