// exercise-images.go

package main

import (
	// "fmt"
	"golang.org/x/tour/pic"
	"image"
	"image/color"
)

/*
@link https://go-zh.org/pkg/image/#Image
type Image interface {
    // ColorModel returns the Image's color model.
    ColorModel() color.Model
    // Bounds returns the domain for which At can return non-zero color.
    // The bounds do not necessarily contain the point (0, 0).
    Bounds() Rectangle
    // At returns the color of the pixel at (x, y).
    // At(Bounds().Min.X, Bounds().Min.Y) returns the upper-left pixel of the grid.
    // At(Bounds().Max.X-1, Bounds().Max.Y-1) returns the lower-right one.
    At(x, y int) color.Color
}
*/
type Image struct {
	W, H int
}

func (i Image) ColorModel() color.Model {
	return color.RGBAModel
}

func (i Image) Bounds() image.Rectangle {
	return image.Rect(0, 0, i.W, i.H)
}

func (i Image) At(x, y int) color.Color {
	cr := uint8(x * y)

	return color.RGBA{cr, cr, 255, 255}

}

func main() {
	m := Image{256, 256}
	pic.ShowImage(m)
}

/*

练习：图片
还记得之前编写的图片生成器吗？现在来另外编写一个，不过这次将会返回 image.Image 来代替 slice 的数据。

自定义的 Image 类型，要实现必要的方法，并且调用 pic.ShowImage。

Bounds 应当返回一个 image.Rectangle，例如 `image.Rect(0, 0, w, h)`。

ColorModel 应当返回 color.RGBAModel。

At 应当返回一个颜色；在这个例子里，在最后一个图片生成器的值 v 匹配 `color.RGBA{v, v, 255, 255}`。
*/
