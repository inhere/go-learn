// exercise-reader.go
package main

import (
	// "fmt"
	"golang.org/x/tour/reader"
)

type MyReader struct{}

func (r MyReader) Read(b []byte) (int, error) {
	n := len(b)
	for i := 0; i < n; i++ {
		b[i] = 'A'
	}

	return n, nil
}

func main() {
	reader.Validate(MyReader{})
}

/*
练习：Reader
实现一个 Reader 类型，它不断生成 ASCII 字符 'A' 的流。
*/
