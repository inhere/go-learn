// errors.go
package main

import (
	"fmt"
	"time"
)

type MyError struct {
	When time.Time
	What string
}

func (e *MyError) Error() string {
	return fmt.Sprintf("at %v , %s", e.When, e.What)
}

// 返回 error 类型。是go内建的接口，定义有 Error() 方法
// *MyError 实现了 Error() 方法，这里就可以认为 *MyError 也是 error 类型的。
func run() error {
	// 要返回指针类型 -- Error 方法是定义在指针类型(*MyError)上的
	// MyError 并没有实现 Error() 方法
	return &MyError{
		time.Now(),
		"it didn't work!",
	}
}

func main() {
	if err := run(); err != nil {
		fmt.Println(err)
	}
}

/*
OUT:
at 2016-05-19 15:20:36.5418589 +0800 CST , it didn't work!

错误
Go 程序使用 error 值来表示错误状态。

与 fmt.Stringer 类似， error 类型是一个内建接口：

type error interface {
    Error() string
}

（与 fmt.Stringer 类似，fmt 包在输出时也会试图匹配 error。）

通常函数会返回一个 error 值，调用的它的代码应当判断这个错误是否等于 nil， 来进行错误处理。

i, err := strconv.Atoi("42")
if err != nil {
    fmt.Printf("couldn't convert number: %v\n", err)
    return
}
fmt.Println("Converted integer:", i)

error 为 nil 时表示成功；非 nil 的 error 表示错误。
*/
